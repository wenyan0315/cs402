# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.

import time
import numpy as np
def matrixMul(A, B,type):
    print("The 1st matrix is ")
    print(A)
    print("The 2nd matrix is ")
    print(B)
    timestamp_1 = time.time()
    if len(A[0]) == len(B):
        res = [[0] * len(B[0]) for i in range(len(A))]
        for i in range(len(A)):
            for j in range(len(B[0])):
                for k in range(len(B)):
                    res[i][j] += A[i][k] * B[k][j]
        timestamp_2 = time.time()
        print("the running time for the multiplication of "+type+" matrices is "+str(timestamp_2-timestamp_1))
        return res

def matrixMul2(A, B,type):
    print("The 1st matrix is ")
    print(A)
    print("The 2nd matrix is ")
    print(B)
    timestamp_1 = time.time()
    if len(A[0]) == len(B):
        C=np.dot(A,B)
        timestamp_2 = time.time()
        print("the running time for the multiplication of "+type+" matrices is "+str(timestamp_2-timestamp_1))
        return C





if __name__ == '__main__':
    A1=np.random.rand(40,30)
    B1=np.random.rand(30,40)
    A2=np.random.randint(0,10,(40,30))
    B2=np.random.randint(0,10,(30,40))

    #mul_1=matrixMul(A1, B1,"real number")
    mul_2 = matrixMul2(A2, B2, "integer")
    print("the result of the multiplication is "+str(mul_2))


